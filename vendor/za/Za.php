<?php
/**
* User: qianjin
* Date: 2015/12/9 0009
* Time: 13:58
*/

class Za{

    public $appName;
    public function __construct($appName){
        $this->appName = $appName;
    }

    static $instance = array();

    static public function getInstance($appName){
        if(isset(static::$instance[$appName])){
            return static::$instance[$appName];
        }
        return static::$instance[$appName] = new Self($appName);
    }

    static public function run($appName){
        $za = static::getInstance($appName);
        try{
            //loader
            $loader = $za->getLoader();

            $loader->add('com', SITE_ROOT);
            $loader->add('za', SITE_ROOT . 'vendor/');
            $loader->add($appName, SITE_ROOT . 'apps/');
            //根据appName 查找配置文件
            $bootstrapName = "\\{$appName}\\Bootstrap";

            if(!class_exists($bootstrapName)){
                throw new \Exception("app {$appName} not found");
            }
            $bootstrap = new $bootstrapName($za);
            $bootstrap->execute();
        } catch(\Exception $e){
            //抛出错误
        }
    }

    protected $loader;

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public function getLoader(){
        if(!$this->loader){
            $this->loader = include(SITE_ROOT . 'vendor/autoload.php');
        }
        return $this->loader;
    }

}
