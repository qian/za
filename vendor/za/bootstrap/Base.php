<?php
/**
 * User: qianjin
 * Date: 2015/12/9 0009
 * Time: 14:36
 */
namespace za\bootstrap;
use za\core\ComponentsManager;

abstract class Base {
    /**
     * @var \za\core\ComponentsManager
     */
    public $di;
    public function __construct($za){
        $this->di = new ComponentsManager();
        $this->set('za', $za);
        $this->set('app', $this);
    }

    public function execute(){
        $this->preRun();
        $this->run();
        $this->afterRun();
    }

    protected function preRun(){}
    protected function afterRun(){}

    abstract public function run();

    protected $loader;

    /**
     * @var \Blade\Factory
     */
    protected $view;
    public function getView(){
        return $this->view;
    }
    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public function getLoader(){
        if(!$this->loader){
            $this->loader = $this->get('za')->getLoader();
        }
        return $this->loader;
    }

    public function set($id, $definition){
        return $this->di->set($id, $definition);
    }

    public function get($id, $throwException = true){
        return $this->di->get($id, $throwException);
    }

}