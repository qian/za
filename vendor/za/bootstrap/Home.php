<?php
namespace za\bootstrap;

use mvc\View;

class Home extends Base{

    public function run(){
        $debugbar = new \DebugBar\StandardDebugBar();
        $debugbarRenderer = $debugbar->getJavascriptRenderer('static/vendor/maximebf/');

        $debugbar->setStorage(new \DebugBar\Storage\FileStorage(SITE_ROOT . '/www/tmp/debug/'));
        $debugbarRenderer->setOpenHandlerUrl('open.php');
//        $debugbar->sendDataInHeaders(true);


        $debugbar["messages"]->addMessage("hello world!");

        try {
            throw new \Exception('foobar');
        } catch (\Exception $e) {
            $debugbar['exceptions']->addException($e);
        }


        $pdoRead  = new \DebugBar\DataCollector\PDO\TraceablePDO(new \PDO("mysql:host=localhost;dbname=test","root","root"));
        $pdoWrite = new \DebugBar\DataCollector\PDO\TraceablePDO(new \PDO("mysql:host=localhost;dbname=test","root","root"));


        $pdoCollector = new \DebugBar\DataCollector\PDO\PDOCollector();
        $pdoCollector->addConnection($pdoRead, 'read-db');
        $pdoCollector->addConnection($pdoWrite, 'write-db');

        $debugbar->addCollector($pdoCollector);

        $pdoRead->query('select * from brands limit 10');

        $pdoRead->query("insert into test values (0, 'test" . rand() . "')");

        $pdoRead->query('select * from brands limit 2');


        $this->view->share('debugbarRenderer', $debugbarRenderer);
        $this->view->share('siteName', '测试网站');
        $view = $this->view->make('header');
        $view->with('arg1', 'aa');

        $view->getEngine()->getCompiler()->directive('datetime', function($timestamp) {
            return preg_replace('/(\(\d+\))/', '<?php echo date("Y-m-d H:i:s", $1); ?>', $timestamp);
        });

        $view->with('time', time());
        $view->with('arg2', 'bbb');
        $view->with('jobs', []);
//        $view->with('jobs', ['job1', 'job2', 'job3']);
        echo $view->render();

        $view = $this->view->make('footer');
        echo $view->render();


    }

    protected function preRun(){
        $vendorDir = SITE_ROOT . 'vendor';
        \Composer\Autoload\includeFile($vendorDir . '/Illuminate/Support/helpers.php');
        $namespace = array(
            'Blade' => $vendorDir,
            'Illuminate' => $vendorDir,
            'mvc' => $vendorDir . '/components/',
        );
        $loader = $this->getLoader();
        foreach ($namespace as $namespace => $path) {
            $loader->set($namespace, $path);
        }

        $this->loadComponents();
        $router = $this->get('router');
        /**@var \Mvc\View $appView */
        $appView = $this->get('view');
        $path = [SITE_ROOT .  'apps/home/views'];
        $view = $appView->getView($path);
        $this->view = $view;
    }

    protected function loadComponents(){
        //加载核心组件
        $this->set('router', function () {

        });

        $this->set('view', function () {
            return new \mvc\View();
        });

        $this->set('response', function () {

        });
    }

}