<?php
/**
 * User: qianjin
 * Date: 2015/12/9 0009
 * Time: 16:48
 */
namespace mvc;

class View{

    protected $view;

    /**
     * @return \Blade\Factory
     */
    public function getView($path){
        if($this->view){
            return $this->view;
        }

        $cachePath = SITE_ROOT . 'data/view_cache_path';
        $compiler = new \Blade\Compilers\BladeCompiler($cachePath);
        $engine = new \Blade\Engines\CompilerEngine($compiler);
        $finder = new \Blade\FileViewFinder($path);
        $factory = new \Blade\Factory($engine, $finder);
        return $this->view = $factory;
    }

}