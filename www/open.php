<?php
define('SITE_ROOT', __DIR__ . '/../');
include(SITE_ROOT . 'vendor/autoload.php');

$debugbar = new \DebugBar\StandardDebugBar();
$debugbar->setStorage(new \DebugBar\Storage\FileStorage(SITE_ROOT . '/www/tmp/debug/'));
$openHandler = new DebugBar\OpenHandler($debugbar);
$openHandler->handle();
